package cn.mrcode.wxsdk.web.zkhelper.curator;

import cn.mrcode.wxsdk.core.context.ConfigContext;
import cn.mrcode.wxsdk.core.dialogue.common.PublicAccount;
import cn.mrcode.wxsdk.core.dialogue.protocol.base.AccessTokenInfo;
import cn.mrcode.wxsdk.web.common.ticket.TicketApi;
import cn.mrcode.wxsdk.web.protocol.base.JsApiTicketInfo;
import com.alibaba.fastjson.JSONObject;
import org.apache.curator.framework.CuratorFramework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  手动重置 zk上的数据
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/28
 */
public class WebResetDataHelper {

    public static void resetTicket(CuratorFramework client, ConfigContext configContext,Map<String, AccessTokenInfo> tokens) throws Exception {
        HashMap<String, PublicAccount> publicAccountMap = configContext.getDirstributedConfig().getPublicAccountMap();

        List<JsApiTicketInfo> data = new ArrayList<JsApiTicketInfo>();
        for ( Map.Entry<String,PublicAccount> entry : publicAccountMap.entrySet()) {
            String key = entry.getKey();
            PublicAccount publicAccount = entry.getValue();

            JsApiTicketInfo jsApiTicketInfo = TicketApi.getJsApiTicketInfo(publicAccount.getAppID(), publicAccount.getAppSecret(), tokens.get(publicAccount.getAppID()).getAccessToken());
            jsApiTicketInfo.setLastUpdateBy(configContext.getId());
            data.add(jsApiTicketInfo);
        }
        client.setData().forPath(ConfigContext.masterPath_ticket, JSONObject.toJSONString(data).getBytes());
    }
}
