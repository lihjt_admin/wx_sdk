package cn.mrcode.wxsdk.web.common.ticket;

import cn.mrcode.wxsdk.core.context.ConfigContext;
import cn.mrcode.wxsdk.core.dialogue.common.exception.ReqException;
import cn.mrcode.wxsdk.core.dialogue.common.exception.WxException;
import cn.mrcode.wxsdk.core.dialogue.common.util.DateUtil;
import cn.mrcode.wxsdk.core.dialogue.service.BaseService;
import cn.mrcode.wxsdk.core.dialogue.service.CommonService;
import cn.mrcode.wxsdk.web.protocol.base.JsApiTicketInfo;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Calendar;
import java.util.Date;


/**
 * 获取 ticket 的api服务
 * @author zhuqiang
 * @version V1.0
 * @date 2015/9/28 10:41
 */
public class TicketApi extends BaseService {
    private static Logger log = LoggerFactory.getLogger(TicketApi.class);
    /** 获得jsapi授权验证码 **/
    public static final String JSAPI_API = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=";

    private TicketApi(){};

    /**
     * 获取 ticket 维护生命周期对象
     * @param appid
     * @param secret
     * @return
     */
    public static JsApiTicketInfo getJsApiTicketInfo(String appid, String secret) throws ReqException, WxException {
        return getJsApiTicketInfoSetUpBy(appid,secret,null);
    }
    /**
     * 获取 ticket 维护生命周期对象
     * @param appid
     * @param secret
     * @param lastUpdateBy 最后更新者
     * @return
     */
    public static JsApiTicketInfo getJsApiTicketInfoSetUpBy(String appid, String secret, String lastUpdateBy) throws ReqException, WxException {
        String ticket =getBaseTicket(appid, secret);
        JsApiTicketInfo result = new JsApiTicketInfo();
        result.setAppID(appid);
        result.setAppSecret(secret);
        result.setJsapi_ticket(ticket);
        result.setCreateTime(new Date());
        result.setTimeOut(DateUtil.computingTime(result.getCreateTime(), Calendar.MINUTE, ConfigContext.JSAPI_TICKET_TIME_OUT));
        result.setLastUpdateBy(lastUpdateBy);
        return result;
    }

    /**
     * 该方法 不依赖于 框架。只要有accesstoken 就能帮你返回
     * @param appid
     * @param secret
     * @param accesstoken
     * @return
     */
    public static JsApiTicketInfo getJsApiTicketInfo(String appid, String secret, String accesstoken) throws ReqException, WxException {
        String ticket =getBaseTicket(appid, secret,accesstoken);
        JsApiTicketInfo result = new JsApiTicketInfo();
        result.setAppID(appid);
        result.setAppSecret(secret);
        result.setJsapi_ticket(ticket);
        result.setCreateTime(new Date());
        result.setTimeOut(DateUtil.computingTime(result.getCreateTime(), Calendar.MINUTE, ConfigContext.JSAPI_TICKET_TIME_OUT));
        return result;
    }

    /**
     * 获取 ticket
     * @param appid
     * @param secret
     * @return
     * @throws ReqException
     */
    private static String getBaseTicket(String appid, String secret) throws ReqException, WxException {
        return getBaseTicket(appid,secret,null);
    }

    /**
     * 获取ticket
     * @param appid
     * @param secret
     * @param accesstoken
     * @return
     * @throws ReqException
     */
    private static String getBaseTicket(String appid, String secret,String accesstoken) throws ReqException, WxException {
        if(StringUtils.isBlank(accesstoken)){
            accesstoken = CommonService.getAccesstoken(appid, secret);
        }
        String api = JSAPI_API.concat(accesstoken).concat("&type=jsapi");
        String resultJson = get(api);
        JSONObject resultObj = JSONObject.parseObject(resultJson);
        handerThrowErrcode(resultObj);
        return resultObj.getString("ticket");
    }
}
