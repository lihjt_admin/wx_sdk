package cn.mrcode.wxsdk.web.protocol.base;

import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * jsapi_ticket 生命周期维护信息
 */
public class JsApiTicketInfo implements Delayed{
	private String appID;  //开发者id
	private String appSecret; //密钥
	private String jsapi_ticket; //js验证授权
	private Date createTime;  //生成的时间
	private Date lastUseTime; //最后一次使用时间
	private String lastUpdateBy; //最后一次更新者
	private Date timeOut; //超时时间

	public String getAppID() {
		return appID;
	}

	public void setAppID(String appID) {
		this.appID = appID;
	}

	public String getAppSecret() {
		return appSecret;
	}

	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}

	public String getJsapi_ticket() {
		return jsapi_ticket;
	}

	public void setJsapi_ticket(String jsapi_ticket) {
		this.jsapi_ticket = jsapi_ticket;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public Date getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(Date timeOut) {
		this.timeOut = timeOut;
	}

	public Date getLastUseTime() {
		return lastUseTime;
	}

	public void setLastUseTime(Date lastUseTime) {
		this.lastUseTime = lastUseTime;
	}

	@Override
	public long getDelay(TimeUnit unit) {
		Date now = new Date();
		long time = this.timeOut.getTime() - now.getTime();  //超时剩余时间。
		return unit.convert(time,TimeUnit.MILLISECONDS); //把毫秒转换为 unit的时间单位
	}

	@Override
	public int compareTo(Delayed o) {
		long time = this.getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
		return time < 0 ? -1 : time > 0 ? 1 : 0;
	}

	public String getLastUpdateBy() {
		return lastUpdateBy;
	}

	public void setLastUpdateBy(String lastUpdateBy) {
		this.lastUpdateBy = lastUpdateBy;
	}
}
