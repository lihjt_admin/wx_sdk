package cn.mrcode.wxsdk.web;

import cn.mrcode.wxsdk.core.test.BaseTest;

/**
 * web 测试基类
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/17 0017 9:50
 */
public class WebBaseTest extends BaseTest {
    public WebBaseTest() {
        super(WebInit.class);
    }
}
