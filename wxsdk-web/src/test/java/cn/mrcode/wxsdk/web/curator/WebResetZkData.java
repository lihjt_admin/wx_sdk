package cn.mrcode.wxsdk.web.curator;

import cn.mrcode.wxsdk.core.context.ConfigContext;
import cn.mrcode.wxsdk.core.dialogue.protocol.base.AccessTokenInfo;
import cn.mrcode.wxsdk.core.zkhelper.curator.ClientHelper;
import cn.mrcode.wxsdk.core.zkhelper.curator.CoreResetDataHelper;
import cn.mrcode.wxsdk.web.WebBaseTest;
import cn.mrcode.wxsdk.web.zkhelper.curator.WebResetDataHelper;
import org.apache.curator.framework.CuratorFramework;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/**
 * 手动重置 zk上的数据,会先调用core的重置数据功能
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/28
 */
public class WebResetZkData extends WebBaseTest{
    private static Logger log = LoggerFactory.getLogger(WebResetZkData.class);
    protected static int sessionTimeout;
    protected static String zkServiceList;

    public WebResetZkData() {
        ConfigContext.DistributedConfig dirstributedConfig = configContext.getDirstributedConfig();
        sessionTimeout = dirstributedConfig.getSessionTimeout();
        zkServiceList = dirstributedConfig.getZkServiceList();
    }

    public void reset() throws Exception {
        CuratorFramework client = ClientHelper.createClient(zkServiceList, sessionTimeout);
        Map<String, AccessTokenInfo> tokens = CoreResetDataHelper.resetToken(client, configContext);
        WebResetDataHelper.resetTicket(client,configContext,tokens);
        client.close();
    }
    public static void main(String[] args) throws Exception {
       new WebResetZkData().reset();
        System.out.println("更新成功！");
        System.exit(0);
    }
}
