package cn.mrcode.wxsdk.web.common;

/**
 * 交易类型
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/24 15:52
 */
public class TradeType {
    /** 支付类型：jsapi*/
    public static final String TRADE_TYPE_JSAPI = "JSAPI";
    /** 支付类型：NATIVE*/
    public static final String TRADE_TYPE_NATIVE ="NATIVE";
    /** 支付类型：APP*/
    public static final String TRADE_TYPE_APP = "APP";
    /** 支付类型：WAP*/
    public static final String TRADE_TYPE_WAP = "WAP";
}
