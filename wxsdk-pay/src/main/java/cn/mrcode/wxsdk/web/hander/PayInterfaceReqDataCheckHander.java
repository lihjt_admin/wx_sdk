package cn.mrcode.wxsdk.web.hander;

import cn.mrcode.wxsdk.core.dialogue.hander.InterfaceReqDataCheckHander;
import cn.mrcode.wxsdk.web.common.TradeType;
import cn.mrcode.wxsdk.web.protocol.req.*;
import org.apache.commons.lang3.StringUtils;


/**
 * 支付接口请求数据检查合法性处理类
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/24 10:58
 */
public class PayInterfaceReqDataCheckHander extends InterfaceReqDataCheckHander {
    /**
     * 对支付接口必要的数据进行非空校验，非空将抛出参数非法异常，所以需要自己先业务校验参数，再调用本方法
     *
     * @param unifiedOrderReqData
     *
     * @return
     */
    public static boolean hander(UnifiedOrderReqData unifiedOrderReqData) {
        String msgPrefix = "统一下单api";
        if (unifiedOrderReqData == null) {
            throw new IllegalArgumentException(msgPrefix + "unifiedOrderReqData对象不能为null");
        }

        String appid = unifiedOrderReqData.getAppid();
        isBlankErr("appid", appid, msgPrefix, "公众账号ID不能为空");
        String mch_id = unifiedOrderReqData.getMch_id();
        isBlankErr("mch_id", mch_id, msgPrefix, "商户号不能为空");
        String nonce_str = unifiedOrderReqData.getNonce_str();
        isBlankErr("nonce_str", nonce_str, msgPrefix, "随机字符串不能为空");
        String sign = unifiedOrderReqData.getSign();
        isBlankErr("sign", sign, msgPrefix, "签名不能为空");
        String body = unifiedOrderReqData.getBody();
        isBlankErr("body", body, msgPrefix, "商品描述不能为空");
        String out_trade_no = unifiedOrderReqData.getOut_trade_no();
        isBlankErr("out_trade_no", out_trade_no, msgPrefix, "商户订单号不能为空");
        String total_fee = unifiedOrderReqData.getTotal_fee();
        isBlankErr("total_fee", total_fee, msgPrefix, "总金额不能为空");
        String spbill_create_ip = unifiedOrderReqData.getSpbill_create_ip();
        isBlankErr("spbill_create_ip", spbill_create_ip, msgPrefix, "终端IP不能为空");
        String notify_url = unifiedOrderReqData.getNotify_url();
        isBlankErr("notify_url", notify_url, msgPrefix, "通知地址不能为空");
        String trade_type = unifiedOrderReqData.getTrade_type();
        isBlankErr("trade_type", trade_type, msgPrefix, "交易类型不能为空");

        if (TradeType.TRADE_TYPE_NATIVE.equals(trade_type)) {
            String product_id = unifiedOrderReqData.getProduct_id();
            isBlankErr("product_id", product_id, msgPrefix, "商品id不能为空");
        }

        if(TradeType.TRADE_TYPE_JSAPI.equals(trade_type)){
            String openid = unifiedOrderReqData.getOpenid();
            isBlankErr("openid",openid,msgPrefix,"在JSAPI交易类型下openid比必传");
        }
        return true;
    }

    /**
     * 订单查询 必填数据非空检查
     *
     * @param orderQueryReqData
     *
     * @return
     */
    public static boolean hander(OrderQueryReqData orderQueryReqData) {
        String msgPrefix = "订单查询api";
        if (orderQueryReqData == null) {
            throw new IllegalArgumentException(msgPrefix + "payQueryReqData对象不能为null");
        }

        String appid = orderQueryReqData.getAppid();
        String mch_id = orderQueryReqData.getMch_id();
        String nonce_str = orderQueryReqData.getNonce_str();
        String sign = orderQueryReqData.getSign();

        handerBaserField(msgPrefix, appid, mch_id, nonce_str, sign);

        String transaction_id = orderQueryReqData.getTransaction_id();
        String out_trade_no = orderQueryReqData.getOut_trade_no();
        if (StringUtils.isBlank(transaction_id)) { //这个为空，商户订单号就必须填写
            isBlankErr("out_trade_no", out_trade_no, msgPrefix, "商户订单号不能为空");
        }
        return true;
    }

    /**
     * 基础的几个校验
     *
     * @param msgPrefix 错误消息前缀，api名称
     * @param appid     公众号id
     * @param mch_id    商户id
     * @param nonce_str 随机字符串
     * @param sign      签名
     */
    private static void handerBaserField(String msgPrefix, String appid, String mch_id, String nonce_str, String sign) {
        isBlankErr("appid", appid, msgPrefix, "公众账号ID不能为空");
        isBlankErr("mch_id", mch_id, msgPrefix, "商户号不能为空");
        isBlankErr("nonce_str", nonce_str, msgPrefix, "随机字符串不能为空");
        isBlankErr("sign", nonce_str, msgPrefix, "签名不能为空");
    }

    /**
     * 关闭订单api校验
     *
     * @param closeOrderReqData
     *
     * @return
     */
    public static boolean hander(CloseOrderReqData closeOrderReqData) {
        String msgPrefix = "关闭订单api";
        String appid = closeOrderReqData.getAppid();
        String mch_id = closeOrderReqData.getMch_id();
        String nonce_str = closeOrderReqData.getNonce_str();
        String sign = closeOrderReqData.getSign();

        String out_trade_no = closeOrderReqData.getOut_trade_no();
        isBlankErr("out_trade_no", out_trade_no, msgPrefix, "商户订单号不能为空");

        handerBaserField(msgPrefix, appid, mch_id, nonce_str, sign);
        return true;
    }

    /**
     * 申请退款
     *
     * @param refundReqData
     *
     * @return
     */
    public static boolean hander(RefundReqData refundReqData) {
        String msgPrefix = "申请退款api";
        String appid = refundReqData.getAppid();
        String mch_id = refundReqData.getMch_id();
        String nonce_str = refundReqData.getNonce_str();
        String sign = refundReqData.getSign();
        handerBaserField(msgPrefix, appid, mch_id, nonce_str, sign);

        String transaction_id = refundReqData.getTransaction_id();
        String out_trade_no = refundReqData.getOut_trade_no();
        if (StringUtils.isBlank(transaction_id)) { //这个为空，商户订单号就必须填写
            isBlankErr("out_trade_no", out_trade_no, msgPrefix, "商户订单号不能为空");
        }
        String out_refund_no = refundReqData.getOut_refund_no();
        isBlankErr("out_refund_no", out_refund_no, msgPrefix, "商户退款单号不能为空");
        String total_fee = refundReqData.getTotal_fee();
        isBlankErr("total_fee", total_fee, msgPrefix, "总金额不能为空");
        String refund_fee = refundReqData.getRefund_fee();
        isBlankErr("refund_fee", refund_fee, msgPrefix, "退款金额不能为空");
        String op_user_id = refundReqData.getOp_user_id();
        isBlankErr("op_user_id", op_user_id, msgPrefix, "操作员不能为空");
        return true;
    }

    /**
     * 退款查询
     * @param refundQueryReqData
     * @return
     */
    public static boolean hander(RefundQueryReqData refundQueryReqData) {
        String msgPrefix = "退款查询api";
        String appid = refundQueryReqData.getAppid();
        String mch_id = refundQueryReqData.getMch_id();
        String nonce_str = refundQueryReqData.getNonce_str();
        String sign = refundQueryReqData.getSign();
        handerBaserField(msgPrefix, appid, mch_id, nonce_str, sign);

        String out_trade_no = refundQueryReqData.getOut_trade_no();
        isBlankErr("out_trade_no", out_trade_no, msgPrefix, "商户系统内部订单号不能为空");
        return true;
    }
}
