package cn.mrcode.wxsdk.web.protocol;

/**
 * 标识接口，支付回包数据
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/26 10:12
 */
public interface PayResData {
}
