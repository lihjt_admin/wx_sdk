package cn.mrcode.wxsdk.web.protocol.res;


import cn.mrcode.wxsdk.web.protocol.PayReqData;

/**
 * 退款查询数据回包
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/26 13:24
 */
public class RefundQueryResData implements PayReqData {
    // 协议 返回结果
    private String return_code; //返回状态码	return_code	是	String(16)	SUCCESS/FAIL 此字段是通信标识，非交易标识，交易是否成功需要查看result_code来判断
    private String return_msg; //返回信息	return_msg	否	String(128)	签名失败返回信息，如非空，为错误原因 ，签名失败 ，参数格式校验错误
    //以下字段在return_code为SUCCESS的时候有返回

    private String result_code; //业务结果	result_code	是	String(16)	SUCCESS SUCCESS/FAIL SUCCESS退款申请接收成功，结果通过退款查询接口查询 FAIL
    private String err_code; //错误码	err_code	是	String(32)	SYSTEMERROR	错误码详见第6节
    private String err_code_des; //错误描述	err_code_des	是	String(32)	系统错误	结果信息描述
    private String appid; //公众账号ID	appid	否	String(32)	wx8888888888888888	微信分配的公众账号ID（企业号corpid即为此appId）
    private String mch_id; //商户号	mch_id	是	String(32)	1900000109	微信支付分配的商户号
    private String device_info; //设备号	device_info	是	String(32)	013467007045764	终端设备号
    private String nonce_str; //随机字符串	nonce_str	是	String(28)	5K8264ILTKCH16CQ2502SI8ZNMTM67VS	随机字符串，不长于32位
    private String sign; //签名	sign	是	String(32)	C380BEC2BFD727A4B6845133519F3AD6	签名，详见签名算法
    private String transaction_id; //微信订单号	transaction_id	是	String(32)	1217752501201407033233368018	微信订单号
    private String out_trade_no; //商户订单号	out_trade_no	是	String(32)	1217752501201407033233368018	商户系统内部的订单号
    private String total_fee; //订单总金额	total_fee	是	Int	100	订单总金额，单位为分，只能为整数，详见支付金额
    private String fee_type; //订单金额货币种类	fee_type	否	String(8)	CNY	订单金额货币类型，符合ISO 4217标准的三位字母代码，默认人民币：CNY，其他值列表详见货币类型
    private String cash_fee; //现金支付金额	cash_fee	是	Int	100	现金支付金额，单位为分，只能为整数，详见支付金额
    private String refund_count; //退款笔数	refund_count	是	Int	1	退款记录数
//    private String out_refund_no_$n; //商户退款单号	out_refund_no_$n	是	String(32)	1217752501201407033233368018	商户退款单号
//    private String refund_id_$n; //微信退款单号	refund_id_$n	是	String(28)	1217752501201407033233368018	微信退款单号
//    private String refund_channel_$n; //退款渠道	refund_channel_$n	否	String(16)	ORIGINAL ORIGINAL—原路退款 BALANCE—退回到余额
//    private String refund_fee_$n; //退款金额	refund_fee_$n	是	Int	100	退款总金额,单位为分,可以做部分退款
//    private String coupon_refund_fee_$n; //代金券或立减优惠退款金额	coupon_refund_fee_$n	否	Int	100	代金券或立减优惠退款金额<=退款金额，退款金额-代金券或立减优惠退款金额为现金，说明详见代金券或立减优惠
//    private String coupon_refund_count_$n; //代金券或立减优惠使用数量	coupon_refund_count_$n	否	Int	1	代金券或立减优惠使用数量 ,$n为下标,从0开始编号
//    private String coupon_refund_batch_id_$n_$m; //代金券或立减优惠批次ID	coupon_refund_batch_id_$n_$m	否	String(20)	100	批次ID ,$n为下标，$m为下标，从0开始编号
//    private String coupon_refund_id_$n_$m; //代金券或立减优惠ID	coupon_refund_id_$n_$m	否	String(20)	10000 	代金券或立减优惠ID, $n为下标，$m为下标，从0开始编号
//    private String coupon_refund_fee_$n_$m; //单个代金券或立减优惠支付金额	coupon_refund_fee_$n_$m	否	Int	100	单个代金券或立减优惠支付金额, $n为下标，$m为下标，从0开始编号
//    private String refund_status_$n; //退款状态	refund_status_$n	是	String(16)	SUCCESS
    /**退款状态：
    SUCCESS—退款成功
    FAIL—退款失败
    PROCESSING—退款处理中
    NOTSURE—未确定，需要商户原退款单号重新发起
    CHANGE—转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者财付通转账的方式进行退款。*/

    public String getReturn_code() {
        return return_code;
    }

    public void setReturn_code(String return_code) {
        this.return_code = return_code;
    }

    public String getReturn_msg() {
        return return_msg;
    }

    public void setReturn_msg(String return_msg) {
        this.return_msg = return_msg;
    }

    public String getResult_code() {
        return result_code;
    }

    public void setResult_code(String result_code) {
        this.result_code = result_code;
    }

    public String getErr_code() {
        return err_code;
    }

    public void setErr_code(String err_code) {
        this.err_code = err_code;
    }

    public String getErr_code_des() {
        return err_code_des;
    }

    public void setErr_code_des(String err_code_des) {
        this.err_code_des = err_code_des;
    }

    public String getAppid() {
        return appid;
    }

    public void setAppid(String appid) {
        this.appid = appid;
    }

    public String getMch_id() {
        return mch_id;
    }

    public void setMch_id(String mch_id) {
        this.mch_id = mch_id;
    }

    public String getDevice_info() {
        return device_info;
    }

    public void setDevice_info(String device_info) {
        this.device_info = device_info;
    }

    public String getNonce_str() {
        return nonce_str;
    }

    public void setNonce_str(String nonce_str) {
        this.nonce_str = nonce_str;
    }

    public String getSign() {
        return sign;
    }

    public void setSign(String sign) {
        this.sign = sign;
    }

    public String getTransaction_id() {
        return transaction_id;
    }

    public void setTransaction_id(String transaction_id) {
        this.transaction_id = transaction_id;
    }

    public String getOut_trade_no() {
        return out_trade_no;
    }

    public void setOut_trade_no(String out_trade_no) {
        this.out_trade_no = out_trade_no;
    }

    public String getTotal_fee() {
        return total_fee;
    }

    public void setTotal_fee(String total_fee) {
        this.total_fee = total_fee;
    }

    public String getFee_type() {
        return fee_type;
    }

    public void setFee_type(String fee_type) {
        this.fee_type = fee_type;
    }

    public String getCash_fee() {
        return cash_fee;
    }

    public void setCash_fee(String cash_fee) {
        this.cash_fee = cash_fee;
    }

    public String getRefund_count() {
        return refund_count;
    }

    public void setRefund_count(String refund_count) {
        this.refund_count = refund_count;
    }
}
