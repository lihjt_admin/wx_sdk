package cn.mrcode.wxsdk.web.protocol;

/**
 * 标识接口，支付请求数据，因为目前 对象转xml 是使用反射，而最简单的就是获取当前类的字段属性。进行数据转换，所以，这个只能是一个标识接口，用于在通用方法处使用
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/26 10:11
 */
public interface PayReqData {
}
