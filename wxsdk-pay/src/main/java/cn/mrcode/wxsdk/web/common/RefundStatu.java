package cn.mrcode.wxsdk.web.common;

/**
 * 退款状态
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/26 14:30
 */
public class RefundStatu {
    public static final String SUCCESS = "SUCCESS"; //SUCCESS—退款成功
    public static final String FAIL = "FAIL";//FAIL—退款失败
    public static final String PROCESSING = "PROCESSING";//PROCESSING—退款处理中
    public static final String NOTSURE = "NOTSURE"; //NOTSURE—未确定，需要商户原退款单号重新发起
    public static final String CHANGE = "CHANGE";//CHANGE—转入代发，退款到银行发现用户的卡作废或者冻结了，导致原路退款银行卡失败，资金回流到商户的现金帐号，需要商户人工干预，通过线下或者财付通转账的方式进行退款。
}
