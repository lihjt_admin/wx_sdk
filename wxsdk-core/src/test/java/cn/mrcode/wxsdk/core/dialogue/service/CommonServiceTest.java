package cn.mrcode.wxsdk.core.dialogue.service;

import cn.mrcode.wxsdk.core.CoreBaseTest;
import org.junit.Test;

/**
 * Created by zhuqiang on 2016/6/15 0015.
 */
public class CommonServiceTest extends CoreBaseTest {
    @Test
    public void getAccesstoken() throws Exception {
        String accesstoken = CommonService.getAccesstoken(publicAccount.getAppID(), publicAccount.getAppSecret());
        System.out.println(accesstoken);
    }
}