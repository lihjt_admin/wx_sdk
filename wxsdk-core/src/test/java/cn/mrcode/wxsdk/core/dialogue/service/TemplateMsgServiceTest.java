package cn.mrcode.wxsdk.core.dialogue.service;

import cn.mrcode.wxsdk.core.CoreBaseTest;
import cn.mrcode.wxsdk.core.dialogue.common.util.DateUtil;
import cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.TemplateReqMsg;
import cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.template.finance_bank.Template_finance_bank_001;
import org.junit.Test;

import java.util.Date;

/**
 * Created by zhuqiang on 2016/6/15 0015.
 */
public class TemplateMsgServiceTest extends CoreBaseTest {

    @Test
    public void send() throws Exception {
        TemplateReqMsg tr = new TemplateReqMsg();
        tr.setTouser("ogR3FuC6wbPcYujdkllotQeReOCw");
        tr.setTemplate_id(Template_finance_bank_001.ID);
        tr.setUrl("www.baidu.com");

        Template_finance_bank_001 t = new Template_finance_bank_001();

        t.setFirst(t.newData("测试标题", "#173177"));
        t.setType(t.newData("订单", "#173177"));
        t.setNum(t.newData("12346", "#173177"));
        t.setAccountType(t.newData("上爱汇","#173177"));
        t.setAccount(t.newData("享优惠", "#173177"));
        t.setTime(t.newData(DateUtil.lFormat(new Date()), "#173177"));
        t.setOrder(t.newData("订单号", "#173177"));
        tr.setData(t);

        String accesstoken = CommonService.getAccesstoken(publicAccount.getAppID(), publicAccount.getAppID());
        TemplateMsgService.send(tr,accesstoken);
    }
}