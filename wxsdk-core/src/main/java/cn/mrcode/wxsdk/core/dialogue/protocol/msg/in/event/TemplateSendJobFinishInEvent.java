package cn.mrcode.wxsdk.core.dialogue.protocol.msg.in.event;


import cn.mrcode.wxsdk.core.dialogue.protocol.msg.InMsg;

/**
 * 模版消息发送完成的 推送事件
 * <xml><ToUserName><![CDATA[gh_4508cd14dac1]]></ToUserName>
     <FromUserName><![CDATA[ogR3FuC6wbPcYujdkllotQeReOCw]]></FromUserName>
     <CreateTime>1444460967</CreateTime>
     <MsgType><![CDATA[event]]></MsgType>
     <Event><![CDATA[TEMPLATESENDJOBFINISH]]></Event>
     <MsgID>213375011</MsgID>
     <Status><![CDATA[success]]></Status>
  </xml>
 * @author zhuqiang
 * @version V1.0
 * @date 2015/10/10 15:18
 */
public class TemplateSendJobFinishInEvent extends InMsg {
    /** 送达成功**/
    public static final String STATUS_SUCCESS = "success";
    /** 送达由于用户拒收（用户设置拒绝接收公众号消息）而失败时 */
    public static final String STATUS_FAILED_USER_BLOCK = "failed:user block";
    /** 送达由于其他原因失败时 */
    public static final String STATUS_FAILED_SYSTEM_FAILED = "failed: system failed";

    public TemplateSendJobFinishInEvent(String toUserName, String fromUserName, Integer createTime, String msgType) {
        super(toUserName, fromUserName, createTime, msgType);
    }

    private String msgID;
    private String event;
    private String status;

    public String getMsgID() {
        return msgID;
    }

    public void setMsgID(String msgID) {
        this.msgID = msgID;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
