package cn.mrcode.wxsdk.core.dialogue.common.util;

import java.util.Properties;

/**
 * @author zhuqiang
 * @version V1.0
 * @Description: 操作系统信息
 * @date 2016/7/04
 */
public class SystemUtil {
    private SystemUtil(){}
    private static Properties sys = System.getProperties();
    /** 操作系统的名称 */
    public static String getOsName(){
        return sys.getProperty("os.name");
    }
    /** 操作系统的版本 */
    public static String getOsVersion(){
        return sys.getProperty("os.version");
    }
}
