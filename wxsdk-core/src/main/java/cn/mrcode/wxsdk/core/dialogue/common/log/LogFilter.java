package cn.mrcode.wxsdk.core.dialogue.common.log;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Title: EH-TP <br>
 * Description: 日志过滤器<br>
 * Date: 2016年01月29日 <br>
 * Copyright (c) 2015 EH <br>
 *
 * @author zhuqiang
 */
public class LogFilter implements Filter {
    private String ignoreSuffix = "gif,png,js,css,ico,jpg";  //忽略追踪的后缀

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest requeest = (HttpServletRequest) servletRequest;

        String requestURI = requeest.getRequestURI();
        int i = requestURI.lastIndexOf(".");
        boolean isTrack = true;
        if (i != -1) {
            String suffix = requestURI.substring(i + 1, requestURI.length() - 1);
            if (ignoreSuffix.contains(suffix)) {
                isTrack = false;
            }
        }
        if (isTrack) {
            LogTrackUtil.start(requeest);
            filterChain.doFilter(servletRequest, servletResponse);
            LogTrackUtil.end();
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
