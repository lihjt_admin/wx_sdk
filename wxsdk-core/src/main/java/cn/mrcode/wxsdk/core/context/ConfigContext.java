package cn.mrcode.wxsdk.core.context;

import cn.mrcode.wxsdk.core.dialogue.common.PublicAccount;
import cn.mrcode.wxsdk.core.dialogue.common.util.IPUtil;
import cn.mrcode.wxsdk.core.dialogue.common.util.SystemUtil;

import java.util.HashMap;

/**
 * 配置 上下文：所有的配置参数都在这里保存
 * @author zhuqiang
 * @version V1.0
 * @date 2016/6/15 16:59
 */
public class ConfigContext {
    /** api获取accesstoken超时时间：单位分钟 */
    public static final int accesstokenTimeOut = 115;
    /** api获取JSAPIticketInfo超时时间：单位分钟 */
    public static final int JSAPI_TICKET_TIME_OUT = 115;
    /** token 子节点 */
    public static final String masterPath_basetoken = "/basetoken";
    /** ticket 子节点 */
    public static final String masterPath_ticket = "/ticket";
    /** 用于存放leader的运行信息 */
    public static final String runIngPath = "/runing";

    private final boolean isDistributedMode;  //是否支持分布式模式
    private HashMap<String, PublicAccount> payPublicAccountMap;//支付公众号信息
    private DistributedConfig dirstributedConfig; //分布式参数配置信息
    private String httpClientClassName; //自定义http请求器
    private String id; //使用本sdk的机器id； 如果为空则使用本机ip地址
    public ConfigContext(boolean isDistributedMode) {
        this.isDistributedMode = isDistributedMode;
    }

    public String getHttpClientClassName() {
        return httpClientClassName;
    }

    public void setHttpClientClassName(String httpClientClassName) {
        this.httpClientClassName = httpClientClassName;
    }

    public DistributedConfig getDirstributedConfig() {
        return dirstributedConfig;
    }

    public void setDirstributedConfig(DistributedConfig dirstributedConfig) {
        this.dirstributedConfig = dirstributedConfig;
    }

    public HashMap<String, PublicAccount> getPayPublicAccountMap() {
        return payPublicAccountMap;
    }

    public void setPayPublicAccountMap(HashMap<String, PublicAccount> payPublicAccountMap) {
        this.payPublicAccountMap = payPublicAccountMap;
    }

    public boolean isDistributedMode() {
        return isDistributedMode;
    }

    public static class DistributedConfig {
        private int sessionTimeout; //zookeeper链接超时时间，单位毫秒
        private String zkServiceList; //zookeeper 服务器列表，中间用 , 逗号隔开
        private HashMap<String, PublicAccount> publicAccountMap; //需要维护的公众号 列表，在使用的时候，所用公众号必须在此列表中

        public int getSessionTimeout() {
            return sessionTimeout;
        }

        public void setSessionTimeout(int sessionTimeout) {
            this.sessionTimeout = sessionTimeout;
        }

        public String getZkServiceList() {
            return zkServiceList;
        }

        public void setZkServiceList(String zkServiceList) {
            this.zkServiceList = zkServiceList;
        }

        public HashMap<String, PublicAccount> getPublicAccountMap() {
            return publicAccountMap;
        }

        public void setPublicAccountMap(HashMap<String, PublicAccount> publicAccountMap) {
            this.publicAccountMap = publicAccountMap;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        if(id == null){
            id = getCurrentIPId();
        }
        this.id = id;
    }

    /** 获取当前机器ip：
     *  ip规则：未获取到有效ip则用“:”分割增加信息
     * */
    public static String getCurrentIPId(){
        String localIP = IPUtil.getLocalIP();
        String hostName = IPUtil.getHostName();
        if(hostName == null){
            hostName = "not getHostName";
        }
        String sysinfo = hostName + ":" + SystemUtil.getOsName() + ":" + SystemUtil.getOsVersion();
        if(localIP == null){
            return "0.0.0.0:not get ip:" + sysinfo;
        }
        return localIP + ":default ip:" + sysinfo;
    }
}
