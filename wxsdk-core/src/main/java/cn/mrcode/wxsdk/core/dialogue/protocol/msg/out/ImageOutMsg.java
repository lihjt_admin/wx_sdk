package cn.mrcode.wxsdk.core.dialogue.protocol.msg.out;


import cn.mrcode.wxsdk.core.dialogue.protocol.msg.InMsg;
import cn.mrcode.wxsdk.core.dialogue.protocol.msg.OutMsg;

/**
 * 回复图片消息 <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>12345678</CreateTime> <MsgType><![CDATA[image]]></MsgType>
 * <Image> <MediaId><![CDATA[media_id]]></MediaId> </Image> </xml>
 */
public class ImageOutMsg extends OutMsg {
	public static final String TEMPLATE = "<xml>\n"
			+ "<ToUserName><![CDATA[${__msg.toUserName}]]></ToUserName>\n"
			+ "<FromUserName><![CDATA[${__msg.fromUserName}]]></FromUserName>\n"
			+ "<CreateTime>${__msg.createTime}</CreateTime>\n"
			+ "<MsgType><![CDATA[${__msg.msgType}]]></MsgType>\n" + "<Image>\n"
			+ "<MediaId><![CDATA[${__msg.mediaId}]]></MediaId>\n"
			+ "</Image>\n" + "</xml>";

	private String mediaId;

	public ImageOutMsg() {
		this.msgType = "image";
	}

	public ImageOutMsg(InMsg inMsg) {
		super(inMsg);
		this.msgType = "image";
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}
}
