package cn.mrcode.wxsdk.core.dialogue.common;


import cn.mrcode.wxsdk.core.dialogue.common.exception.SdkException;
import cn.mrcode.wxsdk.core.dialogue.common.exception.SdkGlobalCode;
import cn.mrcode.wxsdk.core.dialogue.common.exception.WxException;

/**
 * Created by zhuqiang on 2015/8/21.
 */
public class LogUtil {
    /**
     * 按照全局码 格式化日志格式信息
     * @param sdkGlobalCode
     * @return
     */
    public static String fromateLog(String sdkGlobalCode){
        String info = SdkGlobalCode.getInfo(sdkGlobalCode);
        return String.format("%s : %s",sdkGlobalCode,info);
    }
    public static String fromateLog(SdkException e){
        String info = String.format("%s : %s ",e.getCode(),e.getMessage());
        return info;
    }
    public static String fromateLog(WxException e){
        String info = String.format("%s : %s ,接口请求动态返回的Iinfo：%s",e.getCode(),e.getMessage(),e.getDynamicInfo());
        return info;
    }


    public static String printInfoByClassSit(Class<?> cls) {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        String ret = "";
        ret = cls.getSimpleName() + ".java : " + cls.getName() + " Line: " + ste.getLineNumber();
        return ret;
    }

    /**
     * 模拟log4j打印调用处的行数信息；
     * @param cls 调用的类
     * @param value 打印的信息
     * @return
     */
    public static String printInfoByClassSit(Class<?> cls,String value) {
        StackTraceElement ste = new Throwable().getStackTrace()[1];
        String ret = "";
        System.out.println("**********************");
        System.out.println(cls.getName());
        System.out.println(cls.getSimpleName() + ".java");
        System.out.println("Line:" + ste.getLineNumber());
        System.out.println("value:" + value);
        System.out.println("**********************");
        return ret;
    }
}
