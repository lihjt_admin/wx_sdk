package cn.mrcode.wxsdk.core.dialogue.common;

import org.apache.commons.lang3.StringUtils;

/**
 * @author zhuqiang
 * @version V1.0
 * @Description: 消息类型常量,req_为请求消息，res为相应消息类型，evt_为事件类型(该类型是在请求消息为REQ_EVENT前提下的细分事件类型)
 * @date 2015/8/20 9:32
 */
public enum MsgType {
    /** 事件*/
    REQ_EVENT("event"),
    /** 图片*/
    REQ_IMAGE("image"),
    /** 链接*/
    REQ_LINK("link"),
    /** 地理位置消息*/
    REQ_LOCATION("location"),
    /** 文本*/
    REQ_TEXT("text"),
    /** 语音*/
    REQ_VOICE("voice"),
    /** 视频*/
    REQ_VIDEO("video"),
    /** 小视频*/
    REQ_SHORTVIDEO("shortvideo"),


    /** 关注公众号事件*/
    EVT_SUBSCRIBE("subscribe"),
    /** 取消关注公众号事件*/
    EVT_UNSUBSCRIBE("unsubscribe"),
    /** 用户已经关注了的扫码事件*/
    EVT_SCAN("SCAN"),
//    /**
//     * 注意！！！：该事件不是标准的事件类型<br/>
//     * 不能直接用于判断：该事件是用户未关注公众号：扫码同时关注并发起的扫码事件<br/>
//     * 该事件值的组成：EVT_SCAN.value() +  EVT_SUBSCRIBE.value() 的值
//     * */
//    EVT_SCAN_SUBSCRIBE("SCANsubscribe"),
    /** 上报地理位置事件*/
    EVT_LOCATION("LOCATION"),
    /** 自定义菜单事件*/
    EVT_MENU_CLICK("CLICK"),
    /** 点击菜单跳转链接时的事件推送*/
    EVT_MENU_VIEW("VIEW"),
    /** 弹出发送地理位置事件 location_select **/
    EVT_MENU_LOCATION_SELECT("location_select"),
    /** 发送完模版消息，后的事件上报**/
    EVT_TEMPLATESENDJOBFINISH("TEMPLATESENDJOBFINISH"),


    /** 音乐*/
    RES_MUSIC("music"),
    /** 图文*/
    RES_NEWS("news"),
    /** 文本*/
    RES_TEXT("text"),
    /** 图片*/
    RES_IMAGE("image"),
    /** 语音*/
    RES_VOICE("voice"),
    /** 视频*/
    RES_VIDEO("video"),
    /** 多客服转接*/
    RES_TRANSFER_CUSTOMER_SERVICE("transfer_customer_service");


    private String value;

    MsgType(String value) {
        this.value = value;
    }

    public String value() {
        return this.value;
    }

    /**
     * 匹配
     * @param target 与之匹配的消息类型
     * @param msgTypeValue 当前消息类型
     * @return
     */
    public static boolean equals(MsgType target,String msgTypeValue){
        return StringUtils.equalsIgnoreCase(msgTypeValue, target.value());
    }
}
