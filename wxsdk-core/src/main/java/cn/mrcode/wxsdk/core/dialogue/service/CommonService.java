package cn.mrcode.wxsdk.core.dialogue.service;


import cn.mrcode.wxsdk.core.dialogue.common.accessToken.lifeCycle.IAccessTokenService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 常用的api功能请求
 * @author zhuqiang
 * @version V1.0
 * @date 2015/8/19 11:08
 */
public class CommonService extends BaseService {
    private static Logger log = LoggerFactory.getLogger(CommonService.class);
    private static IAccessTokenService iAccessTokenService;

    /**
     * 本方法支持 多公众号获取Accesstoken信息，并且维护Accesstoken的生存时间
     * 如果是分布式的，则不能动态的对新增的 公众号 进行支持
     *
     * @param appid
     * @param secret
     *
     * @return
     */
    public static String getAccesstoken(String appid, String secret) {
        try {
            return iAccessTokenService.getAccesstoken(appid, secret);
        } catch (Exception e) {
            e.printStackTrace();
            String errMsg = "获取token出现了一个致命错误:" + e.getMessage();
            log.error(errMsg);
            throw new RuntimeException(errMsg);
        }
    }
}
