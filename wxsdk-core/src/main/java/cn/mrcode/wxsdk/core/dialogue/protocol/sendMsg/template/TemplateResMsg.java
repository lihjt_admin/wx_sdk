package cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template;

/**
 * 发送模版消息，数据回包
 * @author zhuqiang
 * @version V1.0
 * @date 2015/10/10 11:04
 */
public class TemplateResMsg {
    private String errcode;
    private String errmsg;
    private String msgid;

    public String getErrcode() {
        return errcode;
    }

    public void setErrcode(String errcode) {
        this.errcode = errcode;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getMsgid() {
        return msgid;
    }

    public void setMsgid(String msgid) {
        this.msgid = msgid;
    }
}
