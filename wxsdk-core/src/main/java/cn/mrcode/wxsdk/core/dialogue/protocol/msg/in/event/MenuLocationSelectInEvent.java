package cn.mrcode.wxsdk.core.dialogue.protocol.msg.in.event;


import cn.mrcode.wxsdk.core.dialogue.protocol.msg.InMsg;

/**
 * 发送地理位置 点击菜单触发事件，该事件在 点击菜单之后 地理位置上报事件之后触发
 * @author zhuqiang
 * @version V1.0
 * <xml><ToUserName><![CDATA[gh_e136c6e50636]]></ToUserName>
    <FromUserName><![CDATA[oMgHVjngRipVsoxg6TuX3vz6glDg]]></FromUserName>
    <CreateTime>1408091189</CreateTime>
    <MsgType><![CDATA[event]]></MsgType>
    <Event><![CDATA[location_select]]></Event>
    <EventKey><![CDATA[6]]></EventKey>
    <SendLocationInfo><Location_X><![CDATA[23]]></Location_X>
    <Location_Y><![CDATA[113]]></Location_Y>
    <Scale><![CDATA[15]]></Scale>
    <Label><![CDATA[ 广州市海珠区客村艺苑路 106号]]></Label>
    <Poiname><![CDATA[]]></Poiname>
    </SendLocationInfo>
    </xml>
 * @date 2015/9/10 10:50
 */
public class MenuLocationSelectInEvent extends InMsg {
    private String event;
    private String eventKey;
    private SendLocationInfo sendLocationInfo;//SendLocationInfo	发送的位置信息

    public MenuLocationSelectInEvent(String toUserName, String fromUserName, Integer createTime, String msgType) {
        super(toUserName, fromUserName, createTime, msgType);
    }

    /**
     * 发送的地理位置
     */
    public static class SendLocationInfo{
        private String location_X;//Location_X	X坐标信息
        private String location_Y;//Location_Y	Y坐标信息
        private String scale;//Scale 精度，可理解为精度或者比例尺、越精细的话 scale越高
        private String label;//Label	地理位置的字符串信息
        private String poiname;//Poiname	朋友圈POI的名字，可能为空
        public String getLocation_X() {
            return location_X;
        }

        public void setLocation_X(String location_X) {
            this.location_X = location_X;
        }

        public String getLocation_Y() {
            return location_Y;
        }

        public void setLocation_Y(String location_Y) {
            this.location_Y = location_Y;
        }

        public String getScale() {
            return scale;
        }

        public void setScale(String scale) {
            this.scale = scale;
        }

        public String getLabel() {
            return label;
        }

        public void setLabel(String label) {
            this.label = label;
        }

        public String getPoiname() {
            return poiname;
        }

        public void setPoiname(String poiname) {
            this.poiname = poiname;
        }
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEventKey() {
        return eventKey;
    }

    public void setEventKey(String eventKey) {
        this.eventKey = eventKey;
    }

    public SendLocationInfo getSendLocationInfo() {
        return sendLocationInfo;
    }

    public void setSendLocationInfo(SendLocationInfo sendLocationInfo) {
        this.sendLocationInfo = sendLocationInfo;
    }
}
