package cn.mrcode.wxsdk.core.dialogue.common;

/**
 * Created by zhuqiang on 2015/8/19.
 * 微信公众号配置信息超类
 */
public class PublicAccount {
    /** 开发者配置中心 密钥 */
    private String appSecret;
    //微信分配的公众号ID（开通公众号之后可以获取到）
    private String appID;
    private String token;
    //暂不使用，调试成功后，使用此密钥加解密密文推送消息
    private String encodingAESKey;

    //微信支付分配的商户号ID（开通公众号的微信支付功能之后可以获取到）
    private  String mchID;
    // 商户支付密钥
    private String key;
    //受理模式下给子商户分配的子商户号
    private  String subMchID;

    //HTTPS证书的本地路径
    private  String certLocalPath;

    //HTTPS证书密码，默认密码等于商户号MCHID
    private  String certPassword;


    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEncodingAESKey() {
        return encodingAESKey;
    }

    public void setEncodingAESKey(String encodingAESKey) {
        this.encodingAESKey = encodingAESKey;
    }

    public String getMchID() {
        return mchID;
    }

    public void setMchID(String mchID) {
        this.mchID = mchID;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getSubMchID() {
        return subMchID;
    }

    public void setSubMchID(String subMchID) {
        this.subMchID = subMchID;
    }

    public String getCertLocalPath() {
        return certLocalPath;
    }

    public void setCertLocalPath(String certLocalPath) {
        this.certLocalPath = certLocalPath;
    }

    public String getCertPassword() {
        return certPassword;
    }

    public void setCertPassword(String certPassword) {
        this.certPassword = certPassword;
    }
}
