package cn.mrcode.wxsdk.core.dialogue.service;

import cn.mrcode.wxsdk.core.dialogue.common.exception.ReqException;
import cn.mrcode.wxsdk.core.dialogue.common.exception.WxException;
import cn.mrcode.wxsdk.core.dialogue.hander.DialogueInterfaceReqDataCheckHander;
import cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.TemplateReqMsg;
import cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.TemplateResMsg;
import com.alibaba.fastjson.JSONObject;

/**
 * 模版消息服务
 * @author zhuqiang
 * @version V1.0
 * @date 2015/10/10 11:08
 */
public class TemplateMsgService extends BaseService {
    private static final String SEND_API = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";

    /**
     * 发送模版消息
     * @param templateReqMsg 请求数据包
     * @param accessToken token
     * @return
     * @throws ReqException
     */
    public static TemplateResMsg send(TemplateReqMsg templateReqMsg, String accessToken) throws ReqException, WxException {
        DialogueInterfaceReqDataCheckHander.hander(templateReqMsg);

        String url = SEND_API.concat(accessToken);
        String result = postJson(url, JSONObject.toJSONString(templateReqMsg));
        JSONObject resultObj = JSONObject.parseObject(result);
        handerThrowErrcode(resultObj);
        TemplateResMsg templateResMsg = JSONObject.parseObject(result, TemplateResMsg.class);
        return templateResMsg;
    }
}
