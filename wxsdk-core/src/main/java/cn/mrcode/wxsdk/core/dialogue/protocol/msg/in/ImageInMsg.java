package cn.mrcode.wxsdk.core.dialogue.protocol.msg.in;


import cn.mrcode.wxsdk.core.dialogue.protocol.msg.InMsg;

/**
 * 接收图片消息 <xml> <ToUserName><![CDATA[toUser]]></ToUserName>
 * <FromUserName><![CDATA[fromUser]]></FromUserName>
 * <CreateTime>1348831860</CreateTime> <MsgType><![CDATA[image]]></MsgType>
 * <PicUrl><![CDATA[this is a url]]></PicUrl>
 * <MediaId><![CDATA[media_id]]></MediaId> <MsgId>1234567890123456</MsgId>
 * </xml>
 */
public class ImageInMsg extends InMsg {
	private String picUrl;
	private String mediaId;
	private String msgId;

	public ImageInMsg(String toUserName, String fromUserName,
					  Integer createTime, String msgType) {
		super(toUserName, fromUserName, createTime, msgType);
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getMsgId() {
		return msgId;
	}

	public void setMsgId(String msgId) {
		this.msgId = msgId;
	}
}
