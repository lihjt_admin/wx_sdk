package cn.mrcode.wxsdk.core.dialogue.protocol.sendMsg.template.template;

/**
 * @author zhuqiang
 * @version V1.0
 * @Description: 模版实现类标识
 * @date 2015/10/10 11:26
 */
public class Template {

    public Data newData(String value,String color){
        return new Data(value,color);
    }

    /**
     * 每个字段所对象的值
     */
    public class Data{
        private String value;
        private String color;

        /**
         * 每个字段对应的值
         * @param value 数据
         * @param color 颜色
         */
        public Data(String value, String color) {
            this.value = value;
            this.color = color;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getColor() {
            return color;
        }

        public void setColor(String color) {
            this.color = color;
        }
    }
}
