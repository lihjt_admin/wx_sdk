package cn.mrcode.wxsdk.core.dialogue.protocol.base;

import java.util.Date;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;

/**
 * Created by zhuqiang on 2015/8/19.
 * AccessTokenInfo 信息，用于维护AccessToken 有效时间
 */
public class AccessTokenInfo implements Delayed{
    private String accessToken;  //AccessToken
    private String appID;  //应用id
    private String appSecret; //密钥
    private Date createTime;  //该accesstoken 生成的时间
    private Date lastUseTime; //最后一次使用时间，适用于部分场景
    private String lastUpdateBy; //最后一次更新者
    private Date timeOut;  //该accesstoken超时时间

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAppID() {
        return appID;
    }

    public void setAppID(String appID) {
        this.appID = appID;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public Date getLastUseTime() {
        return lastUseTime;
    }

    public void setLastUseTime(Date lastUseTime) {
        this.lastUseTime = lastUseTime;
    }

    public Date getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Date timeOut) {
        this.timeOut = timeOut;
    }

    @Override
    public long getDelay(TimeUnit unit) {
        Date now = new Date();
        long time = this.timeOut.getTime() - now.getTime();  //超时剩余时间。
        return unit.convert(time,TimeUnit.MILLISECONDS); //把毫秒转换为 unit的时间单位
    }

    @Override
    public int compareTo(Delayed o) {
        long time = this.getDelay(TimeUnit.NANOSECONDS) - o.getDelay(TimeUnit.NANOSECONDS);
        return time < 0 ? -1 : time > 0 ? 1 : 0;
    }

    public String getLastUpdateBy() {
        return lastUpdateBy;
    }

    public void setLastUpdateBy(String lastUpdateBy) {
        this.lastUpdateBy = lastUpdateBy;
    }
}
