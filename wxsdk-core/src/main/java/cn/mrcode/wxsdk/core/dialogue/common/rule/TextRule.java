package cn.mrcode.wxsdk.core.dialogue.common.rule;

/**
 *  文字消息规则
 * @author zhuqiang
 */
public class TextRule extends Rule {
    private String content; //文本消息

    public TextRule() {
    }

    public TextRule(String keyword, String content) {
        super(keyword, Rule.TYPE_TEXT);
        this.content = content;
    }

    public TextRule(String keyword, String type, String content) {
        super(keyword, type);
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
