package cn.mrcode.wxsdk.core.test;

import cn.mrcode.wxsdk.core.Init;
import cn.mrcode.wxsdk.core.context.ConfigContext;
import cn.mrcode.wxsdk.core.dialogue.common.PublicAccount;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

/**
 * 测试基类: 用于提供初始化 配置参数等功能
 * @author zhuqiang
 */
public abstract class BaseTest {
    protected  Logger log = LoggerFactory.getLogger(BaseTest.class);

    protected  PublicAccount publicAccount;
    protected  ConfigContext configContext;

    public BaseTest(Class<? extends Init> initClass) {
        try {
            Init init = initClass.newInstance();
            configContext = init.getConfigContext();
            configContext.setId(ConfigContext.getCurrentIPId() + ":manual update");
            HashMap<String, PublicAccount> publicAccountMap = configContext.getDirstributedConfig().getPublicAccountMap();
            Properties pr = new Properties();
            pr.load(BaseTest.class.getResourceAsStream("/config.properties"));
            String appId1 = (String) pr.get("appId1");
            publicAccount = publicAccountMap.get(appId1);
        } catch (IOException e) {
            log.error("测试配置文件config.properties未找到");
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
