package cn.mrcode.wxsdk.core.dialogue.hander;

import org.apache.commons.lang3.StringUtils;

/**
 * 检测非空和必须传参的处理类超类
 * @author zhuqiang
 * @version V1.0
 * @date 2015/10/10 13:31
 */
public class InterfaceReqDataCheckHander {
    /**
     * 检测非空抛出异常
     *
     * @param key       字段名称
     * @param value     字段值
     * @param msgPrefix 消息前缀（api名称，如： 统一下单api）
     * @param msg       消息提示（如： 公众号id不能为空）
     */
    protected static void isBlankErr(String key, String value, String msgPrefix, String msg) {
        if (StringUtils.isBlank(value)) {
            throw new IllegalArgumentException(msgPrefix + ":" + key + msg);
        }
    }
}
