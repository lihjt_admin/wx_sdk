package cn.mrcode.wxsdk.core.dialogue.common.exception;

/**
 * 纯粹的微信接口返回的异常信息，编码都是从GlobalReturnCode中获取
 * @author zhuqiang
 * @version V1.0
 * @date 2015/11/13 13:37
 */
public class WxException extends SdkException {
    private String dynamicInfo; //动态的错误信息，以免和本地错误对不上，每次请求微信动态的返回的信息
    public WxException(String code) {
        super(code, GlobalReturnCode.getInfo(code));
    }
    public WxException(String code,String dynamicInfo) {
        super(code, GlobalReturnCode.getInfo(code));
        this.dynamicInfo = dynamicInfo;
    }

    public String getDynamicInfo() {
        return dynamicInfo;
    }

    public void setDynamicInfo(String dynamicInfo) {
        this.dynamicInfo = dynamicInfo;
    }
}
