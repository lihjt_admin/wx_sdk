package cn.mrcode.wxsdk.core.dialogue.common.log;

import java.util.Formatter;

/**
 * 日志信息模版工具类
 *
 * @author zhuqiang
 */
public class LogTemplateUtil {
    /**
     * 服务开始
     * @param apiName 服务名称
     * @return
     */
    public static String svStart(String apiName){
        LogTrackReqData logTrackReqData = LogTrackUtil.TRACK_LOCAL.get();
        if(logTrackReqData == null){
            return svFlowStart(apiName,"");
        }
        return svFlowStart(apiName, logTrackReqData.getReqId());
    }

    /**
     * 服务结束
     * @param apiName 服务名称
     * @return
     */
    public static String svEnd(String apiName){
        LogTrackReqData logTrackReqData = LogTrackUtil.TRACK_LOCAL.get();
        if(logTrackReqData != null){
            return svFlowEnd(apiName, logTrackReqData.getReqId());
        }
        return svFlowEnd(apiName, "");
    }

    /**
     * 使用示例：
     * LOG.error(TaskLogUtil.svMsg(apiName, "没有扫描到数据;queryFail返回状态信息=%s", response.getResponseHeader().getResultMessage()));
     * @param apiName
     * @param fromat 提示消息，中间可以用占位符 %s 等
     * @param value 占位符所对应的 值
     * @return
     */
    public static String svMsg(String apiName, String fromat, String...value){
        LogTrackReqData logTrackReqData = LogTrackUtil.TRACK_LOCAL.get();
        if(logTrackReqData != null){
            return svFlowMsg(apiName,  logTrackReqData.getReqId(),fromat,value);
        }else{
            return svFlowMsg(apiName,  "",fromat,value);
        }
    }

    private static String svFlowStart(String apiName, String flowNum){
        return String.format("[%s(%s)]----start",apiName,flowNum);
    }
    private static String svFlowEnd(String apiName, String flowNum){
        return String.format("[%s(%s)]----end",apiName,flowNum);
    }
    /**
     * 打印日志消息
     * @param apiName
     * @param flowNum 处理流水号：用于跟踪一个请求中的请求链操作
     * @param fromat
     * @param value
     * @return
     */
    private static String svFlowMsg(String apiName, String flowNum, String fromat, String...value){
        if(value != null && value.length > 0){
            String[] params = new String[value.length+2];
            params[0] = apiName;
            params[1] = flowNum;
            for (int i = 0; i < value.length; i++) {
                params[2+i] = value[i];
            }
            value = null;
            return new Formatter().format("[%s(%s)]----" + fromat,params).toString();
        }else{
            return new Formatter().format("[%s(%s)]----" + fromat,apiName,flowNum).toString();
        }
    }
}
